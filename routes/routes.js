/**
*
* Archivo para almancear la rutas del aplicativo
*/

// Dependencias
const  testController = require("../controllers/testController");


//inicializacion del objeto principal
 const route = {};
// aca ponemos todas las rutas
 route.createRoutes = function(app){
  //app.use(interceptorController.validarToken);
  app.get("/ws2",testController.test);
  app.get("/consultaAfiliacionesEmpleadores",testController.consultaAfiliacionesEmpleadores);
  app.post("/enviarSATindividual",testController.enviarSATindividual);
  
  
}

 module.exports = route;
 