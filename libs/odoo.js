/**
*
* Archivo que tiene las funcionales de odoo
*
*/

//Dependencias
var Odoo = require('odoo-xmlrpc');
const config = require("../configuracion/config");

//inicializacion del objeti principal

const odooLib = {};

odooLib.inicializarOdoo =  function(usuario,password){
  var odoo = new Odoo({
        url: config.Odoo_DB.url,
        port: config.Odoo_DB.port,
        db: config.Odoo_DB.db,
        username: usuario,
        password: password
    });
    return odoo;
}
odooLib.iniciarConexion = async function(usuario,password,callback){
  // inicializar odooLib
  try{
    var odoo = odooLib.inicializarOdoo(usuario,password);
    await odoo.connect(function (err) {
        if (err) callback(err);
        else callback(true);
    });
  }catch(e){
    callback({"Error":e});
  }
};

module.exports = odooLib;
 