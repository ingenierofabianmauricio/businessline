/**
*
* Archivo que maneja la logica de negocio de las AREAS
*
*/

// Dependencias
const test = require("../dao/testDAO");

// incializacion del objeto general
const testService = {};

testService.test =  function(req,res){
        test.consultar(function(result_sq){
          if(result_sq.Error){
            res.status(500).json({"Error":result_sq.Error});
          }else{
            res.status(200).json(result_sq);
          }
        });
};

testService.consultaAfiliacionesEmpleadores =  function(req,res){
  test.consultaAfiliacionesEmpleadores(function(result_sq){
    if(result_sq.Error){
      res.status(500).json({"Error":result_sq.Error});
    }else{
      res.status(200).json(result_sq);
    }
  });
};


testService.enviarSATindividual =  function(req,res){
  test.enviarSATindividual(req.body,function(result_sq){
    if(result_sq.Error){
      res.status(500).json({"Error":result_sq.Error});
    }else{
      res.status(200).json(result_sq);
    }
  });
};



module.exports = testService;
