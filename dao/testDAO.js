/**
*
* Archivo que tiene las funcionales de usuario de POSTGRESQL
*
*/

//Dependencias

//inicializacion del objeto principal
const dbtest = {};


dbtest.enviarSATindividual = function(body,callback){

  const data = {
    Resultado: 'OK',
    numTransaccion : body.numTransaccion,
    tipoDocIdEmpleador : body.tipoDocIdEmpleador,
    numDocIdEmpleador : body.numDocIdEmpleador,
    serialSatOrgTerritoriales : body.serialSatOrgTerritoriales,
    resultadoTramite : body.resultadoTramite,
    fechaEfectivaAfiliacion : body.fechaEfectivaAfiliacion,
    motivoRechazo : body.motivoRechazo
  };

    // Convertir el objeto JavaScript a una cadena JSON

    // Llamar al callback con la cadena JSON
    callback(data);
}


dbtest.consultar = function(callback){

  const data = {
    transactionId: "0",
    transmissionDateTime: "2024-02-07 18:29:06",
    responseCode: "true",
    authorizationRspCode: "null",
    errorId: "null",
    additionalData: {
        saldo: "89600.0"
    }
};

    // Convertir el objeto JavaScript a una cadena JSON

    // Llamar al callback con la cadena JSON
    callback(data);
}

dbtest.consultaAfiliacionesEmpleadores = function(callback){

    const data = [
        {
        nroCorrelativo: "001",
        estado: "Activo",
        nroRadicado: "RAD-2022-001",
        nroTransaccion: "TRANS-2022-001",
        tipoPersona: "Persona Natural",
        naturalezaJuridicaEmpleador: "Sociedad Anónima",
        tipoDocumentoEmpleador: "NIT",
        noDocumentoEmpleador: "1234567890",
        serialSAT: "456",
        primerNombreEmpleador: "Juan",
        segundoNombreEmpleador: "Pablo",
        primerApellidoEmpleador: "García",
        segundoApellidoEmpleador: "Pérez",
        fechaSolicitud: "2022-01-01",
        perdidaAfiliacionCausaGrave: "false",
        fechaEfectividadAfiliacion: "2022-02-01",
        nombreRazonSocialEmpleador: "Acme Corp",
        noMatriculaMercantil: "123456789",
        dptoSalarios: "11001",
        municipioContacto: "Bogotá",
        direccionContacto: "Calle 123 # 45-67",
        telefonoMunicipioSalarios: "1234567",
        correoElectronicoContacto: "juan.pablo@example.com",
        tipoDocumentoRepresentanteLegal: "Cédula",
        noDocumentoRepresentanteLegal: "12345678",
        primerNombreRepresentanteLegal: "María",
        segundoNombreRepresentanteLegal: "",
        primerApellidoRepresentanteLegal: "Rodríguez",
        segundoApellidoRepresentanteLegal: "Sánchez",
        autorizaDatosPersonales: "true",
        autorizaEnvioNotificaciones: "true",
        manifestacionNoAfiliadoCaja: "false",
        estadoSolicitud: "Asignado al back",
        resultado: "Activo",
        motivoRechazoAfiliacion: "motivoRechazoAfiliacion"
      }, {
        nroCorrelativo: "002",
        estado: "Activo",
        nroRadicado: "RAD-2022-001",
        nroTransaccion: "TRANS-2022-001",
        tipoPersona: "Persona Natural",
        naturalezaJuridicaEmpleador: "Sociedad Anónima",
        tipoDocumentoEmpleador: "NIT",
        noDocumentoEmpleador: "1234567890",
        serialSAT: "456",
        primerNombreEmpleador: "Juan",
        segundoNombreEmpleador: "Pablo",
        primerApellidoEmpleador: "García",
        segundoApellidoEmpleador: "Pérez",
        fechaSolicitud: "2022-01-01",
        perdidaAfiliacionCausaGrave: "false",
        fechaEfectividadAfiliacion: "2022-02-01",
        nombreRazonSocialEmpleador: "Acme Corp",
        noMatriculaMercantil: "123456789",
        dptoSalarios: "11001",
        municipioContacto: "Bogotá",
        direccionContacto: "Calle 123 # 45-67",
        telefonoMunicipioSalarios: "1234567",
        correoElectronicoContacto: "juan.pablo@example.com",
        tipoDocumentoRepresentanteLegal: "Cédula",
        noDocumentoRepresentanteLegal: "12345678",
        primerNombreRepresentanteLegal: "María",
        segundoNombreRepresentanteLegal: "",
        primerApellidoRepresentanteLegal: "Rodríguez",
        segundoApellidoRepresentanteLegal: "Sánchez",
        autorizaDatosPersonales: "true",
        autorizaEnvioNotificaciones: "true",
        manifestacionNoAfiliadoCaja: "false",
        estadoSolicitud: "Asignado al back",
        resultado: "Activo",
        motivoRechazoAfiliacion: "motivoRechazoAfiliacion"
      }, {
        nroCorrelativo: "003",
        estado: "Activo",
        nroRadicado: "RAD-2022-001",
        nroTransaccion: "TRANS-2022-001",
        tipoPersona: "Persona Natural",
        naturalezaJuridicaEmpleador: "Sociedad Anónima",
        tipoDocumentoEmpleador: "NIT",
        noDocumentoEmpleador: "1234567890",
        serialSAT: "456",
        primerNombreEmpleador: "Juan",
        segundoNombreEmpleador: "Pablo",
        primerApellidoEmpleador: "García",
        segundoApellidoEmpleador: "Pérez",
        fechaSolicitud: "2022-01-01",
        perdidaAfiliacionCausaGrave: "false",
        fechaEfectividadAfiliacion: "2022-02-01",
        nombreRazonSocialEmpleador: "Acme Corp",
        noMatriculaMercantil: "123456789",
        dptoSalarios: "11001",
        municipioContacto: "Bogotá",
        direccionContacto: "Calle 123 # 45-67",
        telefonoMunicipioSalarios: "1234567",
        correoElectronicoContacto: "juan.pablo@example.com",
        tipoDocumentoRepresentanteLegal: "Cédula",
        noDocumentoRepresentanteLegal: "12345678",
        primerNombreRepresentanteLegal: "María",
        segundoNombreRepresentanteLegal: "",
        primerApellidoRepresentanteLegal: "Rodríguez",
        segundoApellidoRepresentanteLegal: "Sánchez",
        autorizaDatosPersonales: "true",
        autorizaEnvioNotificaciones: "true",
        manifestacionNoAfiliadoCaja: "false",
        estadoSolicitud: "Asignado al back",
        resultado: "Activo",
        motivoRechazoAfiliacion: "motivoRechazoAfiliacion"
      }];

    // Convertir el objeto JavaScript a una cadena JSON

    // Llamar al callback con la cadena JSON
    callback(data);
}



module.exports = dbtest; 
