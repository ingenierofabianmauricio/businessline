/**
* File for store the properties of the proyects
*
*/

// Dependencias

// incializar el objeto principal
var config ={
    "App":{
    "WebServer":
        {
            "port": 10000,
            "json_spaces" : 2
        }
    },
    "PostGres":{
        //PRODUCCION
        /*
        url_conection: "postgres://hsxyirlsnusqcq:b19ccf896e4086c74c4a4eba9f538603546f238cecb86e52c963f6abf2331ee5@ec2-54-243-128-95.compute-1.amazonaws.com:5432/d7b64vn5h1mplv",
        ssl: true,
        max_conections: 20,
        
        //DESARROLLO
        
        url_conection: "postgres://vzfoofynkbjkup:cfb571f1b481613a0e374706354a751bdd3082fa8378e0d5ae67df7fa1599b47@ec2-54-225-72-238.compute-1.amazonaws.com:5432/d66a0tdpdpf11t",
        ssl: true,
        max_conections: 20,

        */


    //URL OFICIAL PRODUCCIÓN DENTALCORE
       url_conection: "postgres://ankgcnqhyuvgmd:bec3201a55dce0066eae48eb38b356cec5782022afcc09b7eaa1857f27fb5ada@ec2-54-235-114-242.compute-1.amazonaws.com:5432/dejcssuohis35h",
       ssl: true,
       max_conections: 20,
        
        
    },
    "Odoo_DB":{
        /*
        "url": "http://mauricio1.odoo.com/",
        "port": 80,
        "db": "mauricio1"
        */
        
        "url": "http://dental-core.odoo.com",
        "port": 80,
        "db": "dental-core"
    },
    "Odoo_Admin":{
        "user":"contabilidad@dental-core.com",
        "password":"Dentalcore1"
    },
    "tokenSecret":"49022192DFBE5D5C90E72E2CC11DB8C9",
    "excluir_url":["/dentalCore/api/iniciarSesion"]
};

module.exports = config;
